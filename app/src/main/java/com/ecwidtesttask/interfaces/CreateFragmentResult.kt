package com.ecwidtesttask.interfaces

import com.ecwidtesttask.model.Commodity

interface CreateFragmentResult{
    fun pull(code: Int, id: Long?, commodity: Commodity?)
}