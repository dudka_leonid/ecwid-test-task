package com.ecwidtesttask.interfaces

import com.ecwidtesttask.model.Commodity

interface OnActivityDataListener {
    fun onActivityDataListener(code: Int, id: Long?, commodity: Commodity?)
}