package com.ecwidtesttask.interfaces

import com.ecwidtesttask.model.Commodity

interface InterfaceCommunicator {
    fun sendRequestCode(code: Int, id: Long?, commodity: Commodity?)
}