package com.ecwidtesttask.SQL

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.DatabaseUtils
import android.database.sqlite.SQLiteDatabase
import android.util.Log
import com.ecwidtesttask.model.Commodity


class DatabaseAdapter(context: Context) {

    private var dbHelper: DatabaseHelper = DatabaseHelper(context.applicationContext)
    private lateinit var database: SQLiteDatabase

    // Открытие потока БД
    fun open(): DatabaseAdapter {
        database = dbHelper.writableDatabase
        return this
    }

    // Закрытие потока БД
    fun close(){
        dbHelper.close()
    }

    // получаем все записи
    private fun getAllEntries(): Cursor {
        val columns: Array<String> = arrayOf(
            DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_NAME,
            DatabaseHelper.COLUMN_PRICE, DatabaseHelper.COLUMN_IMAGE_URI)
        return  database.query(
            DatabaseHelper.TABLE_NAME, columns, null,
            null, null, null, null)
    }

    // Получаем список всех слов
    fun getAllItems(): ArrayList<Commodity>{
        val commodity: ArrayList<Commodity> = ArrayList()
        val cursor: Cursor = getAllEntries()
        if (cursor.moveToFirst()){
            do {
                val id: Long = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)).toLong()
                val name: String = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_NAME))
                val price: Long = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_PRICE)).toLong()
                val imageURI: String = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_IMAGE_URI))
                
                commodity.add(Commodity(id, name, price, imageURI))
            }while (cursor.moveToNext())
        }
        cursor.close()
        return commodity
    }

    // Получаем кол-во слов
    private fun getCount(): Long{
        return DatabaseUtils.queryNumEntries(database, DatabaseHelper.TABLE_NAME)
    }

    // Получаем слово по id
    fun getItem(id: Long): Commodity?{
        var commodity: Commodity? = null
        val query: String = String
            .format("SELECT * FROM %s WHERE %s=?", DatabaseHelper.TABLE_NAME, DatabaseHelper.COLUMN_ID)
        val cursor: Cursor = database.rawQuery(query, arrayOf(id.toString()))
        if (cursor.moveToFirst()){
            val name: String = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_NAME))
            val price: Long = cursor.getLong(cursor.getColumnIndex(DatabaseHelper.COLUMN_PRICE))
            val imageURI: String = cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_IMAGE_URI))
            commodity = Commodity(id,name, price, imageURI)
        }
        cursor.close()
        return commodity
    }

    fun insertItem(commodity: Commodity): Long{
        val cv = ContentValues()

        cv.put(DatabaseHelper.COLUMN_NAME, commodity.name)
        cv.put(DatabaseHelper.COLUMN_PRICE, commodity.price)
        cv.put(DatabaseHelper.COLUMN_IMAGE_URI, commodity.imageURI)

        return database.insert(DatabaseHelper.TABLE_NAME, null, cv)
    }

    fun delete(commodityId: Long): Int{
        val whereClause = "_id = ?"
        val whereArgs: Array<String> = arrayOf(commodityId.toString())
        return database.delete(DatabaseHelper.TABLE_NAME, whereClause, whereArgs)
    }

    fun deleteAll(): Boolean{
        getAllItems().forEach {
            delete(it.id)
        }
        return getCount() == 0L
    }

    fun update(commodity: Commodity): Int{
        val whereClause: String = DatabaseHelper.COLUMN_ID + "=" + commodity.id.toString()
        val cv = ContentValues()
        cv.put(DatabaseHelper.COLUMN_NAME, commodity.name)
        cv.put(DatabaseHelper.COLUMN_PRICE, commodity.price)
        cv.put(DatabaseHelper.COLUMN_IMAGE_URI, commodity.imageURI)

        return database.update(DatabaseHelper.TABLE_NAME, cv, whereClause, null)
    }

    fun checkRecord(commodity: Commodity): Boolean {
        var ch = false
        val columns: Array<String> = arrayOf(
            DatabaseHelper.COLUMN_ID, DatabaseHelper.COLUMN_NAME,
            DatabaseHelper.COLUMN_PRICE, DatabaseHelper.COLUMN_IMAGE_URI)
        val cursor =  database.query(
            DatabaseHelper.TABLE_NAME, columns,
            DatabaseHelper.COLUMN_NAME+" = ?", arrayOf(commodity.name),
            null, null, null, null)
        if (cursor.count > 0) ch = true
        cursor.close()
        return ch
    }

    fun getIdItem(name: String): Int {
        var id = 0
        val query: String = String
            .format("SELECT * FROM %s WHERE %s=?", DatabaseHelper.TABLE_NAME, DatabaseHelper.COLUMN_NAME)
        val cursor: Cursor = database.rawQuery(query, arrayOf(name))
        if (cursor.moveToFirst()){
            id = cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID))
        }
        cursor.close()
        return id
    }
}