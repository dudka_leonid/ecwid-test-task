package com.ecwidtesttask.SQL

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DatabaseHelper(context: Context)
    :SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "data.db"
        const val TABLE_NAME = "data"

        const val COLUMN_ID = "_id"
        const val COLUMN_NAME = "name"
        const val COLUMN_PRICE = "price"
        const val COLUMN_IMAGE_URI = "image"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("CREATE TABLE " +
                TABLE_NAME + "("
                + COLUMN_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + COLUMN_NAME
                +" TEXT, "
                + COLUMN_PRICE
                + " INTEGER, "
                + COLUMN_IMAGE_URI
                +" TEXT"
                + ")")
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(db)
    }
}