package com.ecwidtesttask

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.NonNull
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentManager
import com.ecwidtesttask.fragments.CommodityListFragment
import com.ecwidtesttask.fragments.CommodityCreateFragment
import com.ecwidtesttask.fragments.dialog.EditCommodityDialogFragment
import com.ecwidtesttask.interfaces.InterfaceCommunicator
import com.ecwidtesttask.interfaces.OnActivityDataListener
import com.ecwidtesttask.model.Commodity
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.floatingactionbutton.FloatingActionButton


class MainActivity : AppCompatActivity(), InterfaceCommunicator{
    // It is master branch

    private val firstFragment: String = "fragment_1"
    private val secondFragment: String = "fragment_2"
    private lateinit var manager: FragmentManager
    private var mListener: OnActivityDataListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val fab = findViewById<FloatingActionButton>(R.id.fab)
        val llBottomSheet = findViewById<View>(R.id.bottom_sheet)
        val bottomSheetBehavior = BottomSheetBehavior.from<View>(llBottomSheet)
        manager = supportFragmentManager
        val ft = manager.beginTransaction()
        ft.add(R.id.frame_container, CommodityCreateFragment(), firstFragment)
        ft.add(R.id.frame_main_container_id, CommodityListFragment(), secondFragment)
        ft.commitNowAllowingStateLoss()
        fab.setOnClickListener {
            when (bottomSheetBehavior.state){
                BottomSheetBehavior.STATE_COLLAPSED -> {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                }
                BottomSheetBehavior.STATE_EXPANDED -> {
                    bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
                }
            }
        }

        bottomSheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(@NonNull bottomSheet: View, newState: Int) {}
            override fun onSlide(@NonNull bottomSheet: View, slideOffset: Float) {
                fab.animate().rotation(slideOffset*45).setDuration(0).start()
            }
        })
    }

    private fun pullManagerFragment(code: Int, id: Long?, commodity: Commodity?){
        mListener = manager.findFragmentByTag(firstFragment) as CommodityCreateFragment
        mListener!!.onActivityDataListener(code, id, commodity)
    }

    override fun sendRequestCode(code: Int, id: Long?, commodity: Commodity?) {
        pullManagerFragment(code, id, commodity)
    }
}
