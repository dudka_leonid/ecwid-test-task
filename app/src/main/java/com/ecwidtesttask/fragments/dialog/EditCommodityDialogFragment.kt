package com.ecwidtesttask.fragments.dialog

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.DialogFragment
import com.ecwidtesttask.R
import com.ecwidtesttask.interfaces.InterfaceCommunicator
import com.ecwidtesttask.model.Commodity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.commodity_edit_fragment.view.*

class EditCommodityDialogFragment(private val commodity: Commodity) : DialogFragment(){

    private var mCallback : InterfaceCommunicator? = null
    private lateinit var nameEditText: EditText
    private lateinit var priceEditText: EditText
    private lateinit var imageView: ImageView
    private var uriNewCommodity: String? = null
    private lateinit var gallery: Intent

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.commodity_edit_fragment, container, false)

        try {
            mCallback = activity as InterfaceCommunicator?
        } catch (e: ClassCastException) {
            throw e
        }

        nameEditText = view.findViewById(R.id.commodity_edit_name_editText_id)
        priceEditText = view.findViewById(R.id.commodity_edit_price_editText_id)
        imageView = view.findViewById(R.id.commodity_edit_imageView_id)

        nameEditText.setText(commodity.name)
        priceEditText.setText(commodity.price.toString())
        Picasso.get().load(Uri.parse(commodity.imageURI)).into(imageView)

        view.btn_dialog_accept.setOnClickListener {
            changeWord()
        }

        view.btn_dialog_delete.setOnClickListener {
            removeWord()
        }

        imageView.setOnClickListener {
            gallery = Intent(Intent.ACTION_OPEN_DOCUMENT)
            gallery.type = "image/*"
            startActivityForResult(gallery, 1)
        }

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1 && resultCode == Activity.RESULT_OK) {
            val uri = data!!.data
            uriNewCommodity = uri.toString()
            Picasso.get().load(Uri.parse(uriNewCommodity)).into(imageView)
        }
    }

    private fun changeWord(){

        var newCommodity: Commodity? = null
        if (nameEditText.text.trim().isNotEmpty() && priceEditText.text.trim().isNotEmpty()){
            newCommodity = Commodity(id = commodity.id, name = nameEditText.text.toString(),
                price = priceEditText.text.toString().toLong(), imageURI = commodity.imageURI)
            if (uriNewCommodity != null){
                newCommodity.imageURI = uriNewCommodity!!
            }
        }
        mCallback!!.sendRequestCode(2, newCommodity?.id, newCommodity)
        dialog!!.dismiss()
    }

    private fun removeWord(){
        mCallback!!.sendRequestCode(3, commodity.id, commodity)
        dialog!!.dismiss()
    }
}