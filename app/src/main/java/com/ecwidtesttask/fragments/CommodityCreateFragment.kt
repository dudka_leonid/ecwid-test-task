package com.ecwidtesttask.fragments

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import androidx.fragment.app.Fragment
import com.ecwidtesttask.MainActivity
import com.ecwidtesttask.R
import com.ecwidtesttask.interfaces.CreateFragmentResult
import com.ecwidtesttask.interfaces.OnActivityDataListener
import com.ecwidtesttask.model.Commodity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.commodity_create_fragment.view.*


class CommodityCreateFragment : Fragment(), OnActivityDataListener {

    private lateinit var imageViewNewCommodity: ImageView
    private lateinit var gallery: Intent
    private var uriNewCommodity: String? = null

    private lateinit var nameEditText: EditText
    private lateinit var priceEditText: EditText

    private var mListener: CreateFragmentResult? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.commodity_create_fragment, container, false)

        imageViewNewCommodity = view.findViewById(R.id.commodity_imageView_id)
        nameEditText = view.findViewById(R.id.commodity_name_EditText_id)
        priceEditText = view.findViewById(R.id.commodity_price_EditText_id)

        mListener = activity!!.supportFragmentManager.findFragmentByTag("fragment_2") as CommodityListFragment

        imageViewNewCommodity.setOnClickListener {
            gallery = Intent(Intent.ACTION_OPEN_DOCUMENT)
            gallery.type = "image/*"
            startActivityForResult(gallery, 1)
        }

        view.save_commodity_button_id.setOnClickListener {

            if (nameEditText.text.trim().isNotEmpty() && priceEditText.text.trim().isNotEmpty()){
                val commodity = Commodity(name = nameEditText.text.toString(),
                    price = priceEditText.text.toString().toLong())

                if (uriNewCommodity != null){
                    commodity.imageURI = uriNewCommodity!!
                }
                mListener!!.pull(1,null, commodity)

                nameEditText.text.clear()
                priceEditText.text.clear()
                imageViewNewCommodity.setImageResource(0)
            }
        }

        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 1 && resultCode == RESULT_OK) {
            val uri = data!!.data
            uriNewCommodity = uri.toString()
            Picasso.get().load(Uri.parse(uriNewCommodity)).into(imageViewNewCommodity)
        }
    }

    override fun onActivityDataListener(code: Int, id: Long?, commodity: Commodity?) {
        mListener!!.pull(code, id, commodity)
    }
}