/*
 * Dudka Leonid
 */


package com.ecwidtesttask.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.ecwidtesttask.R
import com.ecwidtesttask.SQL.DatabaseAdapter
import com.ecwidtesttask.model.Commodity
import com.ecwidtesttask.adapter.CommodityAdapter
import com.ecwidtesttask.interfaces.CreateFragmentResult
import kotlinx.android.synthetic.main.commodity_list_fragment.view.*

class CommodityListFragment: Fragment(), CreateFragmentResult {

    private lateinit var recyclerView: RecyclerView
    private lateinit var databaseAdapter: DatabaseAdapter
    private lateinit var commodity: ArrayList<Commodity>
    private lateinit var commodityAdapter: CommodityAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view: View = inflater.inflate(R.layout.commodity_list_fragment, container, false)

        databaseAdapter = DatabaseAdapter(context!!)
        recyclerView = view.recyclerView
        loadData()

        return view
    }

    private fun loadData(){
        databaseAdapter.open()
        commodity = databaseAdapter.getAllItems()
        databaseAdapter.close()
        commodityAdapter= CommodityAdapter(commodity)
        recyclerView.adapter = commodityAdapter
    }

    override fun pull(code: Int, id: Long?, commodity: Commodity?) {
        when(code){
            // Add Word
            1 -> {
                if (commodity != null){
                    databaseAdapter.open()
                    this.commodity.add(commodity)
                    databaseAdapter.insertItem(commodity)
                    databaseAdapter.close()
                    commodityAdapter.notifyItemInserted(this.commodity.indexOf(commodity))
                }
            }
            // Change Word
            2 -> {
                if (id != null && commodity != null){
                    databaseAdapter.open()
                    this.commodity.forEach {
                        if (it.id == commodity.id){
                           this.commodity[ this.commodity.indexOf(it)] = commodity
                        }
                    }
                    databaseAdapter.update(commodity)
                    databaseAdapter.close()
                    commodityAdapter.notifyItemChanged(this.commodity.indexOf(commodity))
                }
            }
            // Remove Word
            3 -> {
                if (id != null && commodity != null){
                    commodityAdapter.notifyItemRemoved(this.commodity.indexOf(commodity))
                    databaseAdapter.open()
                    this.commodity.removeAt(this.commodity.indexOf(commodity))
                    databaseAdapter.delete(id)
                    databaseAdapter.close()
                }
            }
        }
    }
}