package com.ecwidtesttask.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ecwidtesttask.MainActivity
import com.ecwidtesttask.R
import com.ecwidtesttask.fragments.dialog.EditCommodityDialogFragment
import com.ecwidtesttask.model.Commodity
import com.squareup.picasso.Picasso

class CommodityAdapter(private var commodity: ArrayList<Commodity>)
    : RecyclerView.Adapter<CommodityAdapter.CommodityViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommodityViewHolder {
        val layoutView = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_commodity_recycler_list, parent, false)
        return CommodityViewHolder(layoutView)
    }

    override fun getItemCount(): Int {
        return commodity.size
    }

    override fun onBindViewHolder(holder: CommodityViewHolder, position: Int) {
        holder.bind(commodity[position])
    }

    class CommodityViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        private var name: TextView = itemView.findViewById(R.id.name)
        private var price: TextView = itemView.findViewById(R.id.price)
        private var image: ImageView = itemView.findViewById(R.id.commodity_imageView_id)

        fun bind(commodity: Commodity) {
            name.text = commodity.name
            price.text = commodity.price.toString()
            Picasso.get().load(Uri.parse(commodity.imageURI)).into(image)

            itemView.setOnClickListener {
                EditCommodityDialogFragment(commodity).show((itemView.context as MainActivity).supportFragmentManager,
                    "com.ecwidtesttask.fragments.dialog")
            }
        }
    }
}