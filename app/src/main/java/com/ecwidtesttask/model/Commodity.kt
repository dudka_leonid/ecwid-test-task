/*
 * Dudka Leonid
 */

package com.ecwidtesttask.model

// Main model commodity
data class Commodity(var id: Long = 0, var name: String, var price: Long, var imageURI: String = ""){

    override fun toString(): String {
        return "Commodity(id=$id, name='$name', price=$price, imageURI='$imageURI')"
    }
}